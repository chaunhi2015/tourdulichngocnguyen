<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tdlnn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5]1A53;,VX`d~L1>tfW}^aO]S/&SaF_6>*ee:zX#QebVO3K%FX8.jRR7;v=&.6}{');
define('SECURE_AUTH_KEY',  'OFzn}r4%LQdp4a1(LDV{qjn76Q?3X,V3aIg>bU*.0]yx{@yG,1<j|iKDOW;f#&s ');
define('LOGGED_IN_KEY',    'acz/B-/oV>oF^ G(Tcw!kSGv!Sn97PgveveO$wYR1iJyhH:Y#WkU+UoNoPe|Qow9');
define('NONCE_KEY',        'ex}})^$`fKd>[!BvM^D>VGid^#w#[AvkYUJr{~4U8S5f^s41@rn|`[/R~zi{kYc(');
define('AUTH_SALT',        'tP_3(sQz;VCG<3DA@tj1*nU2-N:|]?(;Gy6^.CP/&8@skc[R*1MDB[i~H59BWaw1');
define('SECURE_AUTH_SALT', 'O&f&PyQq1M|T_mkA<`||}b)6pf[jg-/Sa-NZOZkA%,5da=.z9NPlt$76?J)mytW_');
define('LOGGED_IN_SALT',   'H([Se7DT7g*WUa@[&SG++x;N/OP~:zu>Ov@fIiJsy^O2wh[_vz&yhO9.0H~ D`4y');
define('NONCE_SALT',       'w6mK$KL$@vQ3@HVa5r7u9{4y=a3*&f;$VkYRzW>5^w9uBFxc)tnoi~Gm0OO@,p-@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pys_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
