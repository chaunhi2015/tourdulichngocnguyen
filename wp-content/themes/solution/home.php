<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
	<div id="top-home" ng-controller="advancedSearch" class="ng-scope">
        <div style="margin:auto">
        <?php if ( function_exists('vslider') ) { vslider(); } ?>
        <!--<img src="http://pystravel.vn/wp-content/uploads/2015/05/MC1.jpg" alt="Giới thiệu Tour du lịch PYS Travel" width="1366" height="420">-->
        </div>
        <div id="advancedSearch">
            <form action="<?php echo get_home_url();?>" method="GET" ng-submit="searchNow($event)" class="ng-pristine ng-valid">
                <div class="searchBar">

                    <input type="text" class="form-control input-lg ng-pristine ng-untouched ng-valid ng-empty" placeholder="Nhập nơi bạn muốn đến" id="s" name="s" value="" />
                </div>
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm
                </button>
            </form>
        </div>
    </div>

    <div id="tours">
        <div class="container">
            <div class="row">
                <h2>CÁC TOUR NỔI BẬT</h2>
                <?php
                $arg = array('posts_per_page' => 6,'cat'=>6,'orderby' => 'ID');
                query_posts($arg);
                if(have_posts()):while(have_posts()):the_post();
                    ?>
                    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="wrap">
                                <a href="<?php the_permalink();?>"><img width="306" height="231" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-tour_normal_index wp-post-image" alt="<?php the_title();?>"></a>
                                <div class="tbToursInfo">
                                    <div class="left">
                                        <h4><a href="<?php the_permalink();?>" title="Tour khám phá Hà Giang hùng vĩ"><?php the_title();?></a></h4>
                                        <span class="thoigian">
                                <?php
                                $key_1_value = get_post_meta( get_the_ID(), 'wpcf-thoi-gian', true );
                                // Check if the custom field has a value.
                                if ( ! empty( $key_1_value ) ) {
                                    echo $key_1_value;
                                }
                                ?>
                                </span><span class="gia">
                                <?php
                                $key_1_value = get_post_meta( get_the_ID(), 'wpcf-gia', true );
                                // Check if the custom field has a value.
                                if ( ! empty( $key_1_value ) ) {
                                    echo $key_1_value ."đ";
                                }
                                ?>
                                </span>
                                        <div class="excerpt">
                                            <a href="<?php the_permalink();?>"><p><?php the_excerpt();?></p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <a rel="nofollow" href="<?php the_permalink();?>">Xem thêm <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile;endif;wp_reset_query();?>
            </div>
        </div>
    </div>

    <div id="tours">
        <div class="container">
            <div class="row">
                <h2>CÁC TOUR TRONG NƯỚC</h2>
            		<?php
            			$arg = array('posts_per_page' => 6,'cat'=>4,'orderby' => 'ID');						
						query_posts($arg);
						if(have_posts()):while(have_posts()):the_post();
            		?>
				    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="wrap">
                                <a href="<?php the_permalink();?>"><img width="306" height="231" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-tour_normal_index wp-post-image" alt="<?php the_title();?>"></a>
                                <div class="tbToursInfo">
                                    <div class="left">
                                        <h4><a href="<?php the_permalink();?>" title="Tour khám phá Hà Giang hùng vĩ"><?php the_title();?></a></h4>
                                        <span class="thoigian">
                                    <?php
                                    $key_1_value = get_post_meta( get_the_ID(), 'wpcf-thoi-gian', true );
                                    // Check if the custom field has a value.
                                    if ( ! empty( $key_1_value ) ) {
                                        echo $key_1_value;
                                    }
                                    ?>
                                    </span><span class="gia">
                                    <?php
                                    $key_1_value = get_post_meta( get_the_ID(), 'wpcf-gia', true );
                                    // Check if the custom field has a value.
                                    if ( ! empty( $key_1_value ) ) {
                                        echo $key_1_value ."đ";
                                    }
                                    ?>
                                    </span>
                                        <div class="excerpt">
                                            <a href="<?php the_permalink();?>"><p><?php the_excerpt();?></p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <a rel="nofollow" href="<?php the_permalink();?>">Xem thêm <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;endif;wp_reset_query();?>
			</div>
        </div>
    </div>

    <div id="tours">
        <div class="container">
            <div class="row">
                <h2>CÁC TOUR NƯỚC NGOÀI</h2>
            		<?php
            			$arg = array('posts_per_page' => 6,'cat'=>7,'orderby' => 'ID');						
						query_posts($arg);
						if(have_posts()):while(have_posts()):the_post();
            		?>
				    <div class="col-xs-12 col-lg-4">
                        <div class="row">
                            <div class="wrap">
                                <a href="<?php the_permalink();?>"><img width="306" height="231" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-tour_normal_index wp-post-image" alt="<?php the_title();?>"></a>
                                <div class="tbToursInfo">
                                    <div class="left">
                                        <h4><a href="<?php the_permalink();?>" title="Tour khám phá Hà Giang hùng vĩ"><?php the_title();?></a></h4>
                                        <span class="thoigian">
                                    <?php
                                    $key_1_value = get_post_meta( get_the_ID(), 'wpcf-thoi-gian', true );
                                    // Check if the custom field has a value.
                                    if ( ! empty( $key_1_value ) ) {
                                        echo $key_1_value;
                                    }
                                    ?>
                                    </span><span class="gia">
                                    <?php
                                    $key_1_value = get_post_meta( get_the_ID(), 'wpcf-gia', true );
                                    // Check if the custom field has a value.
                                    if ( ! empty( $key_1_value ) ) {
                                        echo $key_1_value ."đ";
                                    }
                                    ?>
                                    </span>
                                        <div class="excerpt">
                                            <a href="<?php the_permalink();?>"><p><?php the_excerpt();?></p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <a rel="nofollow" href="<?php the_permalink();?>">Xem thêm <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;endif;wp_reset_query();?>
			</div>
        </div>
    </div>


    <div class="slide-tintuc">
        <div class="container">
            <div class="wrapper">
                <h2>Tin tức du lịch</h2>
                <!-- Controls -->
                <div class="control">
                    <a class="left carousel-control" href="#daily-info" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    </a>
                    <a class="right carousel-control" href="#daily-info" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    </a>
                </div>
                <div id="daily-info" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $arg = array('posts_per_page' => 6,'cat'=>5,'orderby' => 'ID');
                        query_posts($arg);$i=0;
                        if(have_posts()):while(have_posts()):the_post();
                            ?>
                            <?php if($i == 0) echo '<div class="item active">'; else if($i % 2 == 0) echo '<div class="item">';?>
                            <div class="col-xs-6">
                                <img width="140" height="100" src="<?php echo get_the_post_thumbnail_url();?>" class="attachment-news_slide_index wp-post-image" alt="<?php the_title();?>">                            <div class="chitiet">
                                    <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>

                                    <?php the_excerpt(100);?> <a rel="nofollow" href="<?php the_permalink();?>">[Xem
                                        chi tiết]</a>

                                </div>
                            </div>
                            <?php $i++;if($i % 2 == 0) echo '</div>'; ?>
                        <?php endwhile;endif;wp_reset_query();?>
                    </div>
                </div>
            </div>
        </div>
        <div id="dk-nhantin">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                </div>
                <div class="col-xs-12 col-lg-4">
                    <h2>Đăng ký nhận tin từ NN Travel</h2>
                    <h4>Mỗi ngày chúng tôi đều gửi đến khách hàng của mình những tin tức thú vị, những ưu đãi hấp
                        dẫn.</h4>
					<form action="#" method="post" id="_form_1038" accept-charset="utf-8" enctype="multipart/form-data" class="ng-pristine ng-valid">
  <input type="hidden" name="f" value="1038">
  <input type="hidden" name="s" value="">
  <input type="hidden" name="c" value="0">
  <input type="hidden" name="m" value="0">
  <input type="hidden" name="act" value="sub">
  <input type="hidden" name="nlbox[]" value="3">
  <div class="_form">
    <div class="formwrapper">
      <div id="_field115">
        <div id="compile115" class="_field _type_input">
          <div class="_label ">
            Họ và tên
          </div>
          <div class="_option">
            <input type="text" name="fullname">
          </div>
        </div>
      </div>
      <div id="_field116">
        <div id="compile116" class="_field _type_input">
          <div class="_label ">
            Email *
          </div>
          <div class="_option">
            <input type="email" name="email">
          </div>
        </div>
      </div>
      <div id="_field117">
        <div id="compile117" class="_field _type_input">
          <div class="_option">
            <input type="submit" value="Đăng ký">
          </div>
        </div>
      </div>
    </div>
    <div class="preview_part">
    </div>
  </div>
</form>                </div>
                <div class="col-xs-12 col-lg-4">
                    <h2>Kết nối với Ngọc Nguyên Travel</h2>
                    <div class="fb-like-box" data-href="https://www.facebook.com/pystravel" data-width="300" data-height="270" data-colorscheme="dark" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>

                </div>
            </div>
        </div>
    </div>
	
	<?php get_footer(); ?>