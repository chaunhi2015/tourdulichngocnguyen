<footer id="footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
    <div class="container">
        <div class="row">
            <!-- Thành tích nổi bật -->
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-3" id="thong-tin-huu-ich">
                <h6>Hoạt động</h6>

                    <?php
                    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=62&echo=0' );
                    if ( $childpages ) {
                        $string = '<ul>' . $childpages . '</ul>';
                    }
                    echo $string;
                    ?>
               
            </div>
            <!-- Thông tin hữu ích -->
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-3" id="thong-tin-huu-ich">
                <h6>Thông tin hữu ích</h6>
                <?php
                $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=71&echo=0' );
                if ( $childpages ) {
                    $string = '<ul>' . $childpages . '</ul>';
                }
                echo $string;
                ?>
            </div>
            <!-- Niềm tự hào của PYS Travel -->
			<div class="col-xs-12 col-md-6 col-sm-6 col-lg-3" id="thong-tin-huu-ich">
                <h6>Danh mục sản phẩm</h6>
				<ul>
                    <?php
                    $child_arg = array( 'hide_empty' => false, 'parent' => 3 );
                    $child_cat = get_terms( 'category', $child_arg );


                    foreach( $child_cat as $child_term ) {
                        echo '<li><a href="'.get_category_link( $child_term->term_id ).'">'.$child_term->name . '</a></li>';
                    }

                    ?>
				</ul>
            </div>
            <!-- Về chúng tôi -->
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-3" id="about-us-ft">
                <h6>Về chúng tôi</h6>

                <p>Với sức trẻ, sự nhiệt huyết và tinh thần luôn luôn đổi mới, Ngọc Nguyên Travel nỗ lực từng ngày để đem đến những sản phẩm du lịch mới mẻ và chất lượng dịch vụ hoàn hảo tới khách hàng
                </p>

                <div class="contact-info">
                    <a href="tel:024.44506070 "><i class="fa fa-mobile"></i><span>08860820 666 </span></a><br>
		<a href="tel:02844506070"><i class="fa fa-mobile"></i><span>0903.460.885</span></a>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="cpn-name" id="ten-cty">
                        <div class="Footer2Column " style="margin-top: 20px;">
                            <p><strong>CÔNG TY TNHH THƯƠNG MẠI VÀ DU LỊCH NGỌC NGUYÊN</strong></p>
                            <p><a href="#">Trụ sở: Số 86 - Ngõ 189 Nguyễn Ngọc Vũ - P.Trung Hòa - Q.Cầu Giấy - TP Hà Nội</a></p>
                            <p><a href="#">Điện thoại: ( 024) 63 299.330      - Fax (024) 3.555.8086</a></p>
                            <p><a href="#">Hotline :0903.460.885 - 0888.999.089</a></p>
                            <p><a href="#">Email: ngocnguyen.xedulich@gmail.com</a></p>
                            <p><a href="#">Website: www.tourdulichngocnguyen.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<style type="text/css">
			.carousel-inner{
				height: 125px;
			}


			.image-1{
				height: 125px !important;
			}
			.image-2{
				height: 110px !important;
				margin-top: 15px;
			}
			.image-5{
				height: 115px !important;
				margin-top: 12px;
			}
			.image-6{
				height: 108px !important;
				margin-top: 15px;
			}
			.image-3{
				height: 125px !important;
				margin-top: 4px;
			}

			/*.image-1 img, .image-2 img, .image-3 img, .image-5 img, .image-6 img{*/
				/*height: 100%;*/
				/*width: 100%;*/
			/*}*/
			/*.image-1, .image-2, .image-3, .image-5, .image-6{
				background: none !important;
				border:0 !important;
			}*/
			.background-border{
				background: none !important;
				border:0 !important;
			}

			.right-1{
				border-radius: 3px;
				background: #dcdcdc;
				position: absolute;
				top:-52px;
				right: 34px;
				width: 30px;
				height: 25px;
			}
			.right-1 i{
				position: absolute;
				top:1px;
				right: 12px;
			}
			.right-2 i{
				position: absolute;
				top:1px;
				right: 12px;
			}
			.right-2{
				border-radius: 3px;
				background: #dcdcdc;
				position: absolute;
				top:-52px;
				right: 0px;
				width: 30px;
				height: 25px;
			}
			.well{
				padding-right: 0 !important;
				padding-left: 0 !important;
			}
			.span1, .span2{
				padding-left: 0;
			}
			.span2:first-child{
				padding-left: 10px;
			}

			.background-tran .thumbnail{
				background: none;
				border:none;
			}
		</style>

    </div>
    <div id="copyright">
        <p><span>Copyright © 2011-2017. All Rights Reserved by Ngoc Nguyen Travel.</span></p>
    </div>

</footer>

<div id="scrollTop" style="position: fixed; bottom: 20px; right: 120px; background: rgba(2, 160, 212, .3); width: 40px; height: 40px; text-align: center; line-height: 40px; color: white; border-radius: 5px;">
    <i class="fa fa-chevron-up"></i></div>

 <script>
    jQuery(document).ready(function ($) {
        $("#scrollTop").click(function () {
            $("html, body").animate({scrollTop: 0}, 500);
        });
    });
</script>

<script
    src="<?php bloginfo('stylesheet_directory') ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script type='text/javascript' src='<?php bloginfo('stylesheet_directory') ?>/js/main.js'></script>

<?php
	wp_footer(); // we need this for plugins
	genesis_after();
?>
</body>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/vendor.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/mutiljs.js"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/mobile-menu.js"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/responsive.css" type="text/css" media="all">
</html>