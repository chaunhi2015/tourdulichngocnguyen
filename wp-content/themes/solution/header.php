<?php
/**
 * WARNING: This file is part of the core Genesis framework. DO NOT edit
 * this file under any circumstances. Please do all modifications
 * in the form of a child theme.
 */
genesis_doctype();

genesis_meta();
?>
<meta content="INDEX,FOLLOW" name="robots"/>
<?php
wp_head(); // we need this for plugins

genesis_title();

?>


<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/bootstrap/css/bootstrap-theme.min.css">
<script src="<?php bloginfo('stylesheet_directory') ?>/bootstrap/js/bootstrap.min.js"></script>


<script href="<?php bloginfo('stylesheet_directory') ?>/js/fontawesome-all.js"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/fontawesome-all.css"/>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/fontawesome-all.min.css"/>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/main.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/minh.css">

<link rel='stylesheet' id='new-css' href='<?php bloginfo('stylesheet_directory') ?>/css/new.css' type='text/css'
      media='all'/>
<link rel='stylesheet' id='mainv2-css' href='<?php bloginfo('stylesheet_directory') ?>/assets/css/main.css'
      type='text/css' media='all'/>

<link rel='stylesheet' id='msl-main-css'
      href='http://pystravel.vn/wp-content/plugins/master-slider/public/assets/css/masterslider.main.css'
      type='text/css' media='all'/>
<link rel='stylesheet' id='msl-custom-css'
      href='http://pystravel.vn/wp-content/uploads/master-slider/custom.css?ver=7.1' type='text/css' media='all'/>

</head>
<body class="wordpress ltr en en-us parent-theme y2018 m05 d11 h04 friday logged-out custom-background custom-header display-header-text plural home blog _masterslider _ms_version_2.6.2 layout-2c-l">
<div class="container-fluid">
    <div class="row">
        <header id="header" role="banner" itemscope="itemscope">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xs-12">
                            <div class="row">
                                <div class="logo-image">
                                    <a href="<?php echo get_home_url(); ?>" title="PYS Travel - Công ty du lịch tiên phong, dẫn đầu về chất lượng dịch vụ">
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/img/logo.png' ?>" title="PYS Travel - Công ty du lịch tiên phong, dẫn đầu về chất lượng dịch vụ"
                                             alt="PYS Travel - Công ty du lịch tiên phong, dẫn đầu về chất lượng dịch vụ">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="row">
                                <div class="hot-line">
                                    <a href="tel:024.44506070 ">
                                        <i class="fa fa-phone"></i>
                                        <span class="lh247">Hotline: 08860820 666</span>
                                        <p class="sdt">ĐT: 0903.460.885</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav id="menu-primary" class="menu hidden-xs hidden-sm" role="navigation" aria-label="Primary Menu" itemscope="itemscope"
                 itemtype="http://schema.org/SiteNavigationElement">
                <div class="container">
                    <div id="topnavbar">
                        <ul id="menu-primary-items" class="menu-items nav navbar-nav">
                            <li id="menu-item-159" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-159">
                                <a href="<?php echo esc_url(get_category_link(3)); ?>">Tour</a>
                            </li>
                            <li id="menu-item-20883"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20883"><a
                                        href="<?php echo esc_url(get_category_link(4)); ?>">Tour Trong Nước</a></li>
                            <li id="menu-item-20883"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20883"><a
                                        href="<?php echo esc_url(get_category_link(7)); ?>">Tour Nước Ngoài</a></li>
                            <li id="menu-item-165"
                                class="menu-item menu-item-type-taxonomy menu-item-object-muc-tin menu-item-165"><a
                                        href="<?php echo esc_url(get_category_link(5)); ?>">Tin du lịch</a></li>
                            <li id="menu-item-16"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16"><a
                                        href="<?php echo get_home_url(); ?>/?page_id=4">Giới Thiệu</a></li>
                            <li id="menu-item-19"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a
                                        href="<?php echo get_home_url(); ?>/?page_id=6">Liên hệ</a></li>
                        </ul>

                        <div id="searchbtn">
                            <i class="fa fa-search"></i>
                        </div>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->

                <!-- top tour -->
                <div id="topTours">
                    <div class="wrapper">
                        <div class="row">
                            <?php
                            $child_arg = array('hide_empty' => false, 'parent' => 4);
                            $child_cat = get_terms('category', $child_arg);

                            foreach ($child_cat as $child_term) {
                                ?>
                                <div class="col-xs-3">
                                    <h4><?php echo $child_term->name; ?></h4>
                                    <a href="<?php echo get_category_link($child_term->term_id); ?>"><img
                                                src="http://pystravel.vn/wp-content/uploads/2015/02/thacbangioc.jpg"
                                                alt="Ba Bể - Bản Giốc" width="220" height="80"></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div id="searcharea">
                    <div class="container">
                        <div class="col-xs-6 col-xs-offset-6">
                            <div id="topbarAdvancedSearch" ng-controller="advancedSearch" class="ng-scope">
                                <form action="<?php echo get_home_url(); ?>" method="get" class="ng-pristine ng-valid">
                                    <div class="searchBar">
                                        <h6>Điểm đến</h6>
                                        <label>Hãy chọn nơi mà bạn muốn đến</label>
                                        <input type="text"
                                               class="form-control input-lg ng-pristine ng-untouched ng-valid ng-empty"
                                               placeholder="Nhập tên thành phố, khu vực..." id="s" name="s" value=""/>

                                    </div>
                                    <button type="submit" class="btn btn-primary" ng-click="searchNow($event); ">
                                        <i class="fa fa-search"></i> Tìm
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="close">
                            <i class="fa fa-times"></i>
                            <p>Đóng</p>
                        </div>
                    </div>
                </div>
            </nav>
            <nav>
                <div id="mod-mobile-menu">
                    <div id="pullMenu" class=""><span class="glyphicon glyphicon glyphicon-align-justify"></span></div>
                    <div class="clearfix"></div>
                    <div id="combineMenu">
                        <div id="closeMenu"></div>
                        <ul id="menu-main-menu" class="mobileMain">
                            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a href="#" class='menu-image-title-after '></a>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <a href="<?php echo esc_url(get_category_link(3)); ?>" class='menu-image-title-after' title="Tour">Tour</a>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($child_cat as $child_term) {
                                        ?>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                            <a target="_blank" href="<?php echo get_category_link($child_term->term_id); ?>" class='menu-image-title-after ' title="<?php echo $child_term->name; ?>">
                                                <?php echo $child_term->name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a href="<?php echo esc_url(get_category_link(4)); ?>" class='menu-image-title-after' title="Tour Trong Nước">Tour Trong Nước</a>
                            </li>
                            <li class=" menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="<?php echo esc_url(get_category_link(7)); ?>" class='menu-image-title-after' title="Tour Nước Ngoài">Tour Nước Ngoài</a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category">
                                <a href="<?php echo esc_url(get_category_link(5)); ?>" class='menu-image-title-after' title="Tin du lịch">Tin du lịch</a>
                            </li>
                            <li class=" menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                <a href="<?php echo get_home_url(); ?>/?page_id=4" class='menu-image-title-after' title="Giới thiệu">Giới Thiệu</a>
                            </li>
                            <li class=" menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                <a href="<?php echo get_home_url(); ?>/?page_id=6" class='menu-image-title-after' title="Liên hệ">Liên hệ</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header><!-- #header -->
    </div>
</div>

