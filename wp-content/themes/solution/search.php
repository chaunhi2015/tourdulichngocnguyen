﻿<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
<div id="search" class="main-section">
        <div class="container">

            <div class="mainTitle">
                <h1>Kết quả tìm kiếm cho từ: "<?php echo $_GET["s"];?>"</h1>
            </div>

                                
                <div class="sectionTitle2">
                    <h2>Tour du lịch</h2>
                </div>
                <?php if(have_posts()):while(have_posts()):the_post();?>
                <?php if (is_search() && ($post->post_type=='page')) continue; ?>
                	<div class="tour">
                        <div class="image">
                            <a rel="nofollow" href="<?php the_permalink();?>" title="<?php the_title();?>"><img width="216" height="185" src="http://pystravel.vn/wp-content/uploads/2018/05/tour-ha-noi-thuong-hai-51-216x185.jpg" class="attachment-tour_cat wp-post-image" alt="tour-ha-noi-thuong-hai-5"></a>
                        </div>
                        <div class="info">
                            <div class="top">
                                <h3 class="title">
                                    <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                        <?php the_title();?>                                   
                                    </a>
                                </h3>
                               
                            </div>
                            <div class="bottom">
                                <div class="left">
                                    <p class="excerpt">
                                    	<?php the_excerpt();?>
                                    </p>
                                    <div class="choices">
                                        <i class="fa fa-check"></i> <span>Bảo hiểm</span><i class="fa fa-street-view"></i> <span>Hướng dẫn viên</span><i class="fa fa-car"></i><span> Xe đưa đón</span><i class="fa fa-ticket"></i> <span>Vé tham quan</span>                                    </div>
                                </div>
                                <div class="right">
                                    <span class="price">
                                        <?php
                                            $key_1_value = get_post_meta( get_the_ID(), 'wpcf-gia', true );
                                            // Check if the custom field has a value.
                                            if ( ! empty( $key_1_value ) ) {
                                                echo $key_1_value;
                                            }
                                        ?>                                    </span>
                                    <span class="price_desc">(VND)</span>
                                    <?php
                                    $ten = get_the_title();
                                    
                                    $gia = get_post_meta( get_the_ID(), 'wpcf-gia', true );

                                    $diem_den = get_post_meta( get_the_ID(), 'wpcf-diem-den', true );
                                    
                                    

                                    $khoi_hanh = get_post_meta( get_the_ID(), 'wpcf-khoi-hanh', true ); 
                                ?>
                                        <?php
                                            $link = "&ten=".$ten."&gia=".$gia."&thoi_gian=".$thoi_gian;
                                        ?>
                                        <a class="btn dat_tour" href="<?php echo get_home_url().'/?page_id=28'.$link;?>">Đặt tour <i class="icon fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endwhile;endif;wp_reset_query();?>

              </div>
    </div>

	<?php get_footer(); ?>