﻿<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<div id="breadcrumbs">

        <div class="wrapper">
            <span prefix="v: http://rdf.data-vocabulary.org/#">
                <span typeof="v:Breadcrumb">
                    <a href="http://pystravel.vn" rel="v:url" property="v:title">Trang chủ</a>
                </span> <i class="fa fa-chevron-right"></i>
                <span typeof="v:Breadcrumb">
                    <a href="" rel="v:url" property="v:title">Liên hệ</a>
                </span>
            </span>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="main-section ">
                <div class="col-xs-12 col-lg-9 col-md-9">
                    <div class="row">
                        <div class="wrapper-contact">
                            <div id="chititet-tour">

                                <h1>Liên hệ</h1>

                            </div>
                            <div id="gioithieu-tour" class="lienhepage">

                                <div class="content">
                                    <h2><strong>Vui lòng để lại câu hỏi, Ngọc Nguyên Travel sẽ liên hệ lại</strong></h2>
                                    <?php
                                    if(have_posts()):while(have_posts()):the_post();
                                        the_content();
                                    endwhile;endif;wp_reset_query();
                                    ?>

                                    <div class="clear"></div>
                                    <div class="row">
                                        <h3 style="font-size:18px; font-weight:bold;">Thông tin liên hệ</h3>
                                        <div class="vertical">
                                            <?php
                                            query_posts("page_id=51&post_type=page");
                                            if(have_posts()):while(have_posts()):the_post();
                                                the_content();
                                            endwhile;endif;wp_reset_query();
                                            ?>
                                        </div>
                                    </div>

                                    <h3>Bản đồ</h3>
                                    <div class="row">
                                        <p style="font-size:18px;font-weight:bold">Trụ sở chính tại Hà Nội</p>
                                        <?php
                                        query_posts("page_id=53&post_type=page");
                                        if(have_posts()):while(have_posts()):the_post();
                                            the_content();
                                        endwhile;endif;wp_reset_query();
                                        ?>

                                    </div>
                                    <div style="padding-top: 30px;">
                                        <p>o <a href="http://pystravel.vn/huong-dan-thanh-toan">Hướng dẫn thanh toán</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-3 col-md-3">
                    <div class="row">
                        <div id="right-sidebar" >
                            <div class="pin-wrapper"><div class="pinned">

                                    <div class="right-col orange">

                                        <div class="header">

                                            Tôi muốn được tư vấn

                                        </div>

                                        <div class="line"></div>

                                        <p>Để lại số điện thoại của bạn, Ngọc Nguyên Travel sẽ gọi cho bạn để tư vấn</p>

                                        <input type="text" placeholder="Số điện thoại của tôi là...">

                                        <button type="submit" class="btn">

                                            Hãy gọi lại cho tôi

                                            <i class="icon fa fa-chevron-right"></i>

                                        </button>

                                    </div>

                                </div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<?php get_footer(); ?>