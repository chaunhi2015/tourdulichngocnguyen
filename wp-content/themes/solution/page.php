<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<div id="breadcrumbs">

      <div class="wrapper">

        <?php
			echo do_shortcode('[breadcrumb]'); 
			?>

      </div>

    </div>

    <div class="main-section container">

        <div class="col-xs-12 col-lg-9 col-md-9">
        	<?php
                    	if(have_posts()):while(have_posts()):the_post();?>
          <div id="chititet-tour">

            <h1><?php the_title();?></h1>
			
          </div>

          <div id="gioithieu-tour" class="lienhepage">

            <div>
            
                <div class="content">

                    
						<?php the_content();?>
						
                    
                </div>

            </div>

          </div>
          <?php endwhile;endif;wp_reset_query();
                    ?>
        </div>

        <div id="right-sidebar" class="col-xs-12 col-lg-3 col-md-3" style="height: 1319px;">
		
			<div class="pin-wrapper"><div class="pinned">

            <div class="right-col orange">

              <div class="header">

                Tôi muốn được tư vấn

              </div>

              <div class="line"></div>

              <p>Để lại số điện thoại của bạn, Ngọc Nguyên Travel sẽ gọi cho bạn để tư vấn</p>

              <input type="text" placeholder="Số điện thoại của tôi là...">

              <button type="submit" class="btn">

                Hãy gọi lại cho tôi

                <i class="icon fa fa-chevron-right"></i>

              </button>

            </div>
			
			</div></div>
      </div>
    </div>
	
	<?php get_footer(); ?>