<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<div id="breadcrumbs">

    <div class="wrapper">

        <?php
            echo do_shortcode('[breadcrumb]'); 
            ?>

    </div>

</div>

<?php
    $categories = get_the_category($post->ID);
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    if(!in_array(5, $category_ids)){
?>

<?php if(have_posts()):while(have_posts()):the_post();?>

<div class="main-section container detail-responsive">

    <div class="col-xs-12 col-lg-9 col-md-9">
        
        <div id="chititet-tour">

            <h1><?php the_title();?></h1>

            <div class="img-thumb-wrapper">

                <img width="708" height="400" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-thumb_single_tour wp-post-image" alt="mocchau-pystravel-1">
            </div>

        </div>

        <div id="gioithieu-tour">

            <div class="wrapper">

                <div class="row">

                    <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 43px;"><div class="col-xs-9 tabtour">

                        <div class="tab select" data-tab="tab1">Giới thiệu chung</div>

                        <div class="tab" data-tab="tab2" onclick="ga('send', 'event', { eventCategory: 'tour_d', eventAction: 'click_tab_lichtrinh'});">
                            Lịch trình chi tiết
                        </div>

                    </div></div>

                    <div class="row">

                        <div class="col-xs-9 main">

                                <div class="content c-tab1" style="display: block">

                                <?php the_content();?>

                                </div>

                                <div class="content c-tab2">

                                  <?php
                                    $key_1_value = get_post_meta( get_the_ID(), 'wpcf-lich-trinh-chi-tiet', true );
                                    // Check if the custom field has a value.
                                    if ( ! empty( $key_1_value ) ) {
                                        echo $key_1_value;
                                    }
                                  ?>
                                </div>

                                <?php
                                    $ten = get_the_title();
                                    
                                    $gia = get_post_meta( get_the_ID(), 'wpcf-gia', true );

                                    $diem_den = get_post_meta( get_the_ID(), 'wpcf-diem-den', true );
                                    
                                    $thoi_gian = get_post_meta( get_the_ID(), 'wpcf-thoi-gian', true );

                                    $khoi_hanh = get_post_meta( get_the_ID(), 'wpcf-khoi-hanh', true ); 
                                ?>
                                <?php endwhile;endif;wp_reset_query();?>
                                
								<!-- banner dưới tour -->
                                <div class="event">
									<a href="#" rel="nofollow">
										<img src="<?php echo get_stylesheet_directory_uri().'/img/banner-du-lich.jpg';?>" alt="Tour du lịch mùa hè 2018">
									</a>		  				
								</div>     
								
                                <!-- Các tour ngẫu nhiên -->
                                <div class="title_related">
                                    <h3 class="heading">Có thể bạn quan tâm</h3>
                                </div>
                                        <div class="releted_post clearfix row">
                                            <?php
                                                $orig_post = $post;
                                                global $post;
                                                $categories = get_the_category($post->ID);
                                                if ($categories) {
                                                $category_ids = array();
                                                foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

                                                $args=array(
                                                'category__in' => $category_ids,
                                                'post__not_in' => array($post->ID),
                                                'posts_per_page'=> 3, // Number of related posts that will be shown.
                                                'caller_get_posts'=>1
                                                );
                                                query_posts($args);
                                                if(have_posts()):while(have_posts()):the_post();
                                                    $gia = get_post_meta( get_the_ID(), 'wpcf-gia', true );

                                                    $diem_den = get_post_meta( get_the_ID(), 'wpcf-diem-den', true );
                                                    
                                                    $thoi_gian = get_post_meta( get_the_ID(), 'wpcf-thoi-gian', true );
                                            ?>
                                            <div class="col-xs-4">
                                                <div class="block">
                                                    <a href="<?php the_permalink();?>" rel="nofollow"><img width="180" height="90" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-sidebar_related_post wp-post-image" alt="da-lat-pys-travel003"></a>
                                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                    <span>Thời gian: <?php if($thoi_gian) echo $thoi_gian; ?></span>
                                                    <span>Giá tour: <?php if($gia) echo $gia; ?> đồng</span>
                                                </div>
                                            </div>
                                             <?php endwhile;endif;wp_reset_query();?>
                                             <?php }?>
                                        </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div id="right-sidebar" class="col-xs-12 col-lg-3 col-md-3">

            <div class="pin-wrapper"><div class="pinned">

                <div class="thongtin" style="height: 280px;">

                    <h2>Thông tin tour</h2>

                    <div class="item">

                        <div class="label">Điểm đến</div>

                        
                        <a href="#"><?php if($diem_den) echo $diem_den; ?></a>
                            
                        
                    </div>

                    <div class="item">

                        <div class="label">Thời gian</div>

                        <a href="#">
                            <?php if($thoi_gian) echo $thoi_gian; ?>                      
                        </a>

                    </div>

                    <div class="item">

                        <div class="label">Khởi hành</div>

                        <a href="#">
                            <?php if($khoi_hanh) echo $khoi_hanh; ?>
                        </a>

                    </div>


                    <div class="price">
                                       <?php if($gia) echo $gia; ?>
                    </div>

                    <div class="btn" id="button_dat_tour">
                        <?php
                            $link = "&ten=".$ten."&gia=".$gia."&thoi_gian=".$thoi_gian;
                        ?>
                        <a href="<?php echo get_home_url().'/?page_id=28'.$link;?>">
                            Đặt tour ngay

                            <i class="fa fa-chevron-right"></i>

                        </a>
                    </div>

                </div>

                	<div class="right-col orange" onclick="_gaq.push(['_trackEvent', 'mWebCall', 'Click2Call']);">
		<div class="header">
			Gọi để được tư vấn
		</div>

		<div class="line"></div>
		<div class="goiDienTuVan_phoneBlock">
			<div class="iconPhone">
				<i class="fa fa-phone"></i>
			</div>
			<div class="bigger">
				<p>Hotline: <span>08860820 666</span></p>
			</div>
			<div class="smaller">
				<p>ĐT: <span>0903.460.885</span></p>
			</div>
		</div>
		
	</div>
	
            </div></div>
        </div>
    </div>
	<?php }else{ ?>

<?php if(have_posts()):while(have_posts()):the_post();?>

<div class="main-section container detail-responsive" id="chitiettin">

        <div class="col-xs-9">

            <div>
              <h1><?php the_title();?></h1>

              <div id="excerpt-thumbnail" class="clearfix">
                    <div class="thumbnail-tin">
                      <img width="280" height="170" src="<?php echo get_the_post_thumbnail_url();?>" class="attachment-thumb_single_tin wp-post-image" alt="<?php the_title();?>">                
                  </div>
                     <?php the_excerpt();?>           
                </div>
              <div id="content">
                    <?php the_content();?>

              </div>
            </div>
            <!-- Banner Event duoi bai viet  -->
            <div class="event">
                <a href="#" rel="nofollow">
                    <img src="https://i.imgur.com/T3Wj6BD.png" alt="Tour du lịch mùa hè 2018">
                </a>
            </div>
    <?php endwhile;endif;wp_reset_query();?>

           <div class="tinngaunhien_block" style="padding: 20px;">
               <!-- Các tin ngẫu nhiên -->
           
               <div class="title_related">
                    <h3 class="heading">Có thể bạn quan tâm</h3>
                </div>
                 <div class="releted_post clearfix row">

                    <?php

                        $args=array(
                        'category__in' => $category_ids,
                        'post__not_in' => array($post->ID),
                        'posts_per_page'=> 3, // Number of related posts that will be shown.
                        'caller_get_posts'=>1
                        );
                        query_posts($args);
                        if(have_posts()):while(have_posts()):the_post();
                    ?>
                     <div class="col-xs-4">
                        <div class="block">
                           <a href="<?php the_permalink();?>" rel="nofollow">
                            <img width="180" height="90" src="<?php echo get_the_post_thumbnail_url();?>" class="attachment-sidebar_related_post wp-post-image" alt="<?php the_title();?>"></a>
                           <a href="<?php the_permalink();?>"><?php the_title();?></a>
                        </div>
                    </div>
                     <?php endwhile;endif;wp_reset_query();?>             
                                    
                </div>           
            </div>
         
        
           
           <!-- Bài viết mới nhất -->
           
           <div class="tinmoinhat clearfix"><div class="tin-comment">Những tin du lịch mới nhất</div>
           <ul>
                <?php 
                    query_posts("cat=5&posts_per_page=10&order=ID");
                    if(have_posts()):while(have_posts()):the_post();
                ?>
                <li>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                </li>
                <?php endwhile;endif;wp_reset_query();?>
            </ul>
        </div>           
           
           

        </div>

        <div id="right-sidebar" class="col-xs-3">
          

          <div class="wrap" id="lienhetuvan">
              <div class="right-col orange">

                <div class="header">
                    Gọi để được tư vấn
                </div>

                <div class="line"></div>
                <img class="tvdt_img" src="http://pystravel.vn/wp-content/themes/pysvn_based_on_hybrid/img/tvdt.png" alt="Liên hệ tư vấn" onclick="_gaq.push(['_trackEvent', 'mWebCall', 'Click2Call']);">

              </div>

          </div>

             
    <div class="wrap" id="dangkynhantin">
        <div class="right-col">

            <div class="header title_sidebar">

              Đăng ký nhận tin

            </div>

            <p>Hãy là người đầu tiên nhận được những tin tức thú vị và những chương trình khuyến mại nóng hổi nhất.</p>

            <form action="#" method="post" id="_form_1038" accept-charset="utf-8" enctype="multipart/form-data" class="ng-pristine ng-valid">
                  
                  <div class="_form">
                    <div class="formwrapper">
                      <div id="_field115">
                        <div id="compile115" class="_field _type_input">
                          <div class="_label ">
                            Họ và tên
                          </div>
                          <div class="_option">
                            <input type="text" name="fullname">
                          </div>
                        </div>
                      </div>
                      <div id="_field116">
                        <div id="compile116" class="_field _type_input">
                          <div class="_label ">
                            Email *
                          </div>
                          <div class="_option">
                            <input type="email" name="email">
                          </div>
                        </div>
                      </div>
                      <div id="_field117">
                        <div id="compile117" class="_field _type_input">
                          <div class="_option">
                            <input type="submit" value="Đăng ký">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="preview_part">
                    </div>
                  </div>
                </form>
          </div>
    </div>

        </div>
    </div>



    <?php }?>
	<?php get_footer(); ?>