<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 

	<div id="breadcrumbs">

        <div class="wrapper">

          <?php
			echo do_shortcode('[breadcrumb]'); 
			?>
        </div>

      </div>

<div id="danhsach-tin">

		<div class="wrapper">

			<div class="row">

                <div class="col-xs-12 archive-title">
                    <h1>Các tin du lịch hấp dẫn và mới nhất</h1>
                </div>

				<div class="col-xs-12 col-lg-9 col-md-9">

					<!-- danh sach cac tour -->
                    
                        
                                
						<?php if(have_posts()):while(have_posts()):the_post();?>
							<div class="cac-tin">

								<div class="col-lg-4 col-xs-12 col-md-4">
                                    <div class="row">
                                        <div class="img">
                                            <a href="<?php the_permalink();?>"><img width="216" height="185" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-tour_cat wp-post-image" alt="<?php the_title();?>"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-4 col-xs-12">
                                    <div class="row">
                                        <div class="thongtin">

                                            <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>

                                            <div class="line"></div>

                                            <div class="text"><?php echo get_excerpt(200); ?></div>

                                            <div class="line"></div>

                                            <div class="index">

                                                <span><i class="fa fa-calendar"></i> 10/05/2018</span>

                                                <a href="<?php the_permalink();?>" title="Xem chi tiết">[ Xem chi tiết ]</a>

                                            </div>

                                        </div>
                                    </div>
                                </div>
							</div>

						<?php endwhile;endif;wp_reset_query();?>
					
					<div id="paginate">
					    <div class="wp-pagenavi">
					    	<?php wp_pagenavi();?>
						</div>					
					</div>

				</div>

				<div class="col-xs-12 col-lg-3 col-md-3">
					<div class="right-col orange">

						<div class="header">
                            Gọi để được tư vấn
                        </div>

                        <div class="line"></div>
                        
												<img class="tvdt_img" src="<?php echo get_stylesheet_directory_uri().'/img/121.png'?>" alt="Liên hệ tư vấn" onclick="_gaq.push(['_trackEvent', 'mWebCall', 'Click2Call']);">
						
                        <p>Hoặc để lại số điện thoại, Ngọc Dương Travel sẽ gọi cho bạn</p>

                        <div role="form" class="wpcf7" id="wpcf7-f531-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/tour#wpcf7-f531-o1" method="post" class="wpcf7-form ng-pristine ng-valid" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="531">
<input type="hidden" name="_wpcf7_version" value="4.2.2">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f531-o1">
<input type="hidden" name="_wpnonce" value="2fb346123b">
</div>
<p><span class="wpcf7-form-control-wrap tel-904"><input type="tel" name="tel-904" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại của tôi là..."></span><br>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal-tuvan">Tiếp tục</button></p>
<div class="modal modal-tuvan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-sm">
<div class="modal-content">
<p>Vui lòng nhập mã xác thực (phân biệt chữ hoa/chữ thường)</p>
<p>    <input type="hidden" name="_wpcf7_captcha_challenge_captcha-867" value="138297314"><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-867" width="72" height="24" alt="captcha" src="http://pystravel.vn/wp-content/uploads/wpcf7_captcha/138297314.png"><span class="wpcf7-form-control-wrap captcha-867"><input type="text" name="captcha-867" value="" size="40" class="wpcf7-form-control wpcf7-captchar" aria-invalid="false" placeholder="Mã xác thực"></span><br>
    <input type="submit" value="Hãy gọi lại cho tôi" class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="http://pystravel.vn/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;">
    </p></div>
<p style="height: 20px;"></p></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
					</div>
				</div>

			</div>

		</div>

	  </div>

	
	<?php get_footer(); ?>