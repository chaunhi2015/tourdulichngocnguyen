jQuery(document).ready(function ($) {
    $("#menu-item-7693").remove();
    $('#right-sidebar').height($('.main-section.container').height() - 100);
    /*$(".pinned").pin({
        containerSelector: "#right-sidebar"
    });*/
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $(".scrolltop").fadeIn('slow');
        }
        else {
            $(".scrolltop").fadeOut('slow');
        }
    });

    $(".scrolltop").click(function () {
        $("html, body").animate({scrollTop: 0}, 'slow');
    });
    $("#menu-item-159 > a").hover(function (event) {
        // $("#topTours").show().mouseleave(function(event) {
        // 	$(this).css('display', 'none').css({
        // 		property1: 'value1',
        // 		property2: 'value2'
        // 	});;
        // });
        // $(this).parent().toggleClass('active');
        $("#searcharea").css('display', 'none');
        $("#topTours").css('display', 'block');
        $("#topTours").mouseleave(function (event) {
            $(this).css('display', 'none');
        });
    });
    $(".menu-item:not(#menu-item-159) a").mouseenter(function (event) {
        $("#topTours").css('display', 'none');
    })

    $("#searchbtn").tooltip({
        placement: 'left',
        title: 'Tìm tour',
        trigger: 'manual'
    }).tooltip('show');
    $("#searchbtn").click(function () {
        $("#searcharea").fadeToggle();
        $("topTours").css('display', 'none');
    });

    $("#gioithieu-tour .tab").click(function () {
        var tab = $(this).attr("data-tab");
        $(this).addClass("select");
        $(this).siblings().removeClass("select");
        $(".content").hide();
        $(".c-" + tab).show();
    });

    //fix display error
    $("p:has(img.aligncenter)").find("img").css({"display": "block", "margin": "0 auto"});
    $("p").each(function (index) {
        if ($(this).html() == "") $(this).height(20);
    });

    //$("#right-sidebar").stick_in_parent();

    //numberToStar("#tours > div > div > div.col-xs-8 > div > div > div.right > div");
    //numberToStar("#chititet-tour > div > div > div.col-xs-3.thongtin > div:nth-child(5) > a > div");
    
    $("#searcharea .close").click(function () {
        $("#searcharea").fadeOut();
    });

    $(window).resize(function () {
        $("#menu-primary").css('width', $(window).width() + 'px');
    });
});

function formatNumber(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}
