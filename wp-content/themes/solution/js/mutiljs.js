(function (B) {
    B.extend(B.fn, {
        validate: function (E) {
            if (!this.length) {
                E && E.debug && window.console && console.warn("nothing selected, can't validate, returning nothing");
                return
            }
            var A = B.data(this[0], "validator");
            if (A) {
                return A
            }
            this.attr("novalidate", "novalidate");
            A = new B.validator(E, this[0]);
            B.data(this[0], "validator", A);
            if (A.settings.onsubmit) {
                var F = this.find("input, button");
                F.filter(".cancel").click(function () {
                    A.cancelSubmit = true
                });
                if (A.settings.submitHandler) {
                    F.filter(":submit").click(function () {
                        A.submitButton = this
                    })
                }
                this.submit(function (D) {
                    if (A.settings.debug) {
                        D.preventDefault()
                    }
                    function C() {
                        if (A.settings.submitHandler) {
                            if (A.submitButton) {
                                var H = B("<input type='hidden'/>").attr("name", A.submitButton.name).val(A.submitButton.value).appendTo(A.currentForm)
                            }
                            A.settings.submitHandler.call(A, A.currentForm, D);
                            if (A.submitButton) {
                                H.remove()
                            }
                            return false
                        }
                        return true
                    }

                    if (A.cancelSubmit) {
                        A.cancelSubmit = false;
                        return C()
                    }
                    if (A.form()) {
                        if (A.pendingRequest) {
                            A.formSubmitted = true;
                            return false
                        }
                        return C()
                    } else {
                        A.focusInvalid();
                        return false
                    }
                })
            }
            return A
        }, valid: function () {
            if (B(this[0]).is("form")) {
                return this.validate().form()
            } else {
                var D = true;
                var A = B(this[0].form).validate();
                this.each(function () {
                    D &= A.element(this)
                });
                return D
            }
        }, removeAttrs: function (F) {
            var E = {}, A = this;
            B.each(F.split(/\s/), function (D, C) {
                E[C] = A.attr(C);
                A.removeAttr(C)
            });
            return E
        }, rules: function (K, N) {
            var R = this[0];
            if (K) {
                var L = B.data(R.form, "validator").settings;
                var P = L.rules;
                var O = B.validator.staticRules(R);
                switch (K) {
                    case"add":
                        B.extend(O, B.validator.normalizeRule(N));
                        P[R.name] = O;
                        if (N.messages) {
                            L.messages[R.name] = B.extend(L.messages[R.name], N.messages)
                        }
                        break;
                    case"remove":
                        if (!N) {
                            delete P[R.name];
                            return O
                        }
                        var Q = {};
                        B.each(N.split(/\s/), function (D, C) {
                            Q[C] = O[C];
                            delete O[C]
                        });
                        return Q
                }
            }
            var A = B.validator.normalizeRules(B.extend({}, B.validator.metadataRules(R), B.validator.classRules(R), B.validator.attributeRules(R), B.validator.staticRules(R)), R);
            if (A.required) {
                var M = A.required;
                delete A.required;
                A = B.extend({required: M}, A)
            }
            return A
        }
    });
    B.extend(B.expr[":"], {
        blank: function (A) {
            return !B.trim("" + A.value)
        }, filled: function (A) {
            return !!B.trim("" + A.value)
        }, unchecked: function (A) {
            return !A.checked
        }
    });
    B.validator = function (A, D) {
        this.settings = B.extend(true, {}, B.validator.defaults, A);
        this.currentForm = D;
        this.init()
    };
    B.validator.format = function (A, D) {
        if (arguments.length == 1) {
            return function () {
                var C = B.makeArray(arguments);
                C.unshift(A);
                return B.validator.format.apply(this, C)
            }
        }
        if (arguments.length > 2 && D.constructor != Array) {
            D = B.makeArray(arguments).slice(1)
        }
        if (D.constructor != Array) {
            D = [D]
        }
        B.each(D, function (F, C) {
            A = A.replace(new RegExp("\\{" + F + "\\}", "g"), C)
        });
        return A
    };
    B.extend(B.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: B([]),
            errorLabelContainer: B([]),
            onsubmit: true,
            ignore: ":hidden",
            ignoreTitle: false,
            onfocusin: function (A, D) {
                this.lastActive = A;
                if (this.settings.focusCleanup && !this.blockFocusCleanup) {
                    this.settings.unhighlight && this.settings.unhighlight.call(this, A, this.settings.errorClass, this.settings.validClass);
                    this.addWrapper(this.errorsFor(A)).hide()
                }
            },
            onfocusout: function (A, D) {
                if (!this.checkable(A) && (A.name in this.submitted || !this.optional(A))) {
                    this.element(A)
                }
            },
            onkeyup: function (A, D) {
                if (A.name in this.submitted || A == this.lastElement) {
                    this.element(A)
                }
            },
            onclick: function (A, D) {
                if (A.name in this.submitted) {
                    this.element(A)
                } else {
                    if (A.parentNode.name in this.submitted) {
                        this.element(A.parentNode)
                    }
                }
            },
            highlight: function (F, E, A) {
                if (F.type === "radio") {
                    this.findByName(F.name).addClass(E).removeClass(A)
                } else {
                    B(F).addClass(E).removeClass(A)
                }
            },
            unhighlight: function (F, E, A) {
                if (F.type === "radio") {
                    this.findByName(F.name).removeClass(E).addClass(A)
                } else {
                    B(F).removeClass(E).addClass(A)
                }
            }
        },
        setDefaults: function (A) {
            B.extend(B.validator.defaults, A)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: B.validator.format("Please enter no more than {0} characters."),
            minlength: B.validator.format("Please enter at least {0} characters."),
            rangelength: B.validator.format("Please enter a value between {0} and {1} characters long."),
            range: B.validator.format("Please enter a value between {0} and {1}."),
            max: B.validator.format("Please enter a value less than or equal to {0}."),
            min: B.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: false,
        prototype: {
            init: function () {
                this.labelContainer = B(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || B(this.currentForm);
                this.containers = B(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();
                var E = (this.groups = {});
                B.each(this.settings.groups, function (D, C) {
                    B.each(C.split(/\s/), function (I, J) {
                        E[J] = D
                    })
                });
                var F = this.settings.rules;
                B.each(F, function (D, C) {
                    F[D] = B.validator.normalizeRule(C)
                });
                function A(C) {
                    var D = B.data(this[0].form, "validator"), H = "on" + C.type.replace(/^validate/, "");
                    D.settings[H] && D.settings[H].call(D, this[0], C)
                }

                B(this.currentForm).validateDelegate("[type='text'], [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'] ", "focusin focusout keyup", A).validateDelegate("[type='radio'], [type='checkbox'], select, option", "click", A);
                if (this.settings.invalidHandler) {
                    B(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler)
                }
            }, form: function () {
                this.checkForm();
                B.extend(this.submitted, this.errorMap);
                this.invalid = B.extend({}, this.errorMap);
                if (!this.valid()) {
                    B(this.currentForm).triggerHandler("invalid-form", [this])
                }
                this.showErrors();
                return this.valid()
            }, checkForm: function () {
                this.prepareForm();
                for (var A = 0, D = (this.currentElements = this.elements()); D[A]; A++) {
                    this.check(D[A])
                }
                return this.valid()
            }, element: function (D) {
                D = this.validationTargetFor(this.clean(D));
                this.lastElement = D;
                this.prepareElement(D);
                this.currentElements = B(D);
                var A = this.check(D) !== false;
                if (A) {
                    delete this.invalid[D.name]
                } else {
                    this.invalid[D.name] = true
                }
                if (!this.numberOfInvalids()) {
                    this.toHide = this.toHide.add(this.containers)
                }
                this.showErrors();
                return A
            }, showErrors: function (D) {
                if (D) {
                    B.extend(this.errorMap, D);
                    this.errorList = [];
                    for (var A in D) {
                        this.errorList.push({message: D[A], element: this.findByName(A)[0]})
                    }
                    this.successList = B.grep(this.successList, function (C) {
                        return !(C.name in D)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            }, resetForm: function () {
                if (B.fn.resetForm) {
                    B(this.currentForm).resetForm()
                }
                this.submitted = {};
                this.lastElement = null;
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass(this.settings.errorClass)
            }, numberOfInvalids: function () {
                return this.objectLength(this.invalid)
            }, objectLength: function (F) {
                var A = 0;
                for (var E in F) {
                    A++
                }
                return A
            }, hideErrors: function () {
                this.addWrapper(this.toHide).hide()
            }, valid: function () {
                return this.size() == 0
            }, size: function () {
                return this.errorList.length
            }, focusInvalid: function () {
                if (this.settings.focusInvalid) {
                    try {
                        B(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                    } catch (A) {
                    }
                }
            }, findLastActive: function () {
                var A = this.lastActive;
                return A && B.grep(this.errorList, function (D) {
                    return D.element.name == A.name
                }).length == 1 && A
            }, elements: function () {
                var D = this, A = {};
                return B(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function () {
                    !this.name && D.settings.debug && window.console && console.error("%o has no name assigned", this);
                    if (this.name in A || !D.objectLength(B(this).rules())) {
                        return false
                    }
                    A[this.name] = true;
                    return true
                })
            }, clean: function (A) {
                return B(A)[0]
            }, errors: function () {
                return B(this.settings.errorElement + "." + this.settings.errorClass, this.errorContext)
            }, reset: function () {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = B([]);
                this.toHide = B([]);
                this.currentElements = B([])
            }, prepareForm: function () {
                this.reset();
                this.toHide = this.errors().add(this.containers)
            }, prepareElement: function (A) {
                this.reset();
                this.toHide = this.errorsFor(A)
            }, check: function (A) {
                A = this.validationTargetFor(this.clean(A));
                var J = B(A).rules();
                var M = false;
                for (var N in J) {
                    var K = {method: N, parameters: J[N]};
                    try {
                        var I = B.validator.methods[N].call(this, A.value.replace(/\r/g, ""), A, K.parameters);
                        if (I == "dependency-mismatch") {
                            M = true;
                            continue
                        }
                        M = false;
                        if (I == "pending") {
                            this.toHide = this.toHide.not(this.errorsFor(A));
                            return
                        }
                        if (!I) {
                            this.formatAndAdd(A, K);
                            return false
                        }
                    } catch (L) {
                        this.settings.debug && window.console && console.log("exception occured when checking element " + A.id + ", check the '" + K.method + "' method", L);
                        throw L
                    }
                }
                if (M) {
                    return
                }
                if (this.objectLength(J)) {
                    this.successList.push(A)
                }
                return true
            }, customMetaMessage: function (E, F) {
                if (!B.metadata) {
                    return
                }
                var A = this.settings.meta ? B(E).metadata()[this.settings.meta] : B(E).metadata();
                return A && A.messages && A.messages[F]
            }, customMessage: function (A, F) {
                var E = this.settings.messages[A];
                return E && (E.constructor == String ? E : E[F])
            }, findDefined: function () {
                for (var A = 0; A < arguments.length; A++) {
                    if (arguments[A] !== undefined) {
                        return arguments[A]
                    }
                }
                return undefined
            }, defaultMessage: function (A, D) {
                return this.findDefined(this.customMessage(A.name, D), this.customMetaMessage(A, D), !this.settings.ignoreTitle && A.title || undefined, B.validator.messages[D], "<strong>Warning: No message defined for " + A.name + "</strong>")
            }, formatAndAdd: function (F, H) {
                var A = this.defaultMessage(F, H.method), G = /\$?\{(\d+)\}/g;
                if (typeof A == "function") {
                    A = A.call(this, H.parameters, F)
                } else {
                    if (G.test(A)) {
                        A = jQuery.format(A.replace(G, "{$1}"), H.parameters)
                    }
                }
                this.errorList.push({message: A, element: F});
                this.errorMap[F.name] = A;
                this.submitted[F.name] = A
            }, addWrapper: function (A) {
                if (this.settings.wrapper) {
                    A = A.add(A.parent(this.settings.wrapper))
                }
                return A
            }, defaultShowErrors: function () {
                for (var A = 0; this.errorList[A]; A++) {
                    var E = this.errorList[A];
                    this.settings.highlight && this.settings.highlight.call(this, E.element, this.settings.errorClass, this.settings.validClass);
                    this.showLabel(E.element, E.message)
                }
                if (this.errorList.length) {
                    this.toShow = this.toShow.add(this.containers)
                }
                if (this.settings.success) {
                    for (var A = 0; this.successList[A]; A++) {
                        this.showLabel(this.successList[A])
                    }
                }
                if (this.settings.unhighlight) {
                    for (var A = 0, F = this.validElements(); F[A]; A++) {
                        this.settings.unhighlight.call(this, F[A], this.settings.errorClass, this.settings.validClass)
                    }
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show()
            }, validElements: function () {
                return this.currentElements.not(this.invalidElements())
            }, invalidElements: function () {
                return B(this.errorList).map(function () {
                    return this.element
                })
            }, showLabel: function (A, F) {
                var E = this.errorsFor(A);
                if (E.length) {
                    E.removeClass(this.settings.validClass).addClass(this.settings.errorClass);
                    E.attr("generated") && E.html(F)
                } else {
                    E = B("<" + this.settings.errorElement + "/>").attr({
                        "for": this.idOrName(A),
                        generated: true
                    }).addClass(this.settings.errorClass).html(F || "");
                    if (this.settings.wrapper) {
                        E = E.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()
                    }
                    if (!this.labelContainer.append(E).length) {
                        this.settings.errorPlacement ? this.settings.errorPlacement(E, B(A)) : E.insertAfter(A)
                    }
                }
                if (!F && this.settings.success) {
                    E.text("");
                    typeof this.settings.success == "string" ? E.addClass(this.settings.success) : this.settings.success(E)
                }
                this.toShow = this.toShow.add(E)
            }, errorsFor: function (D) {
                var A = this.idOrName(D);
                return this.errors().filter(function () {
                    return B(this).attr("for") == A
                })
            }, idOrName: function (A) {
                return this.groups[A.name] || (this.checkable(A) ? A.name : A.id || A.name)
            }, validationTargetFor: function (A) {
                if (this.checkable(A)) {
                    A = this.findByName(A.name).not(this.settings.ignore)[0]
                }
                return A
            }, checkable: function (A) {
                return /radio|checkbox/i.test(A.type)
            }, findByName: function (A) {
                var D = this.currentForm;
                return B(document.getElementsByName(A)).map(function (F, C) {
                    return C.form == D && C.name == A && C || null
                })
            }, getLength: function (D, A) {
                switch (A.nodeName.toLowerCase()) {
                    case"select":
                        return B("option:selected", A).length;
                    case"input":
                        if (this.checkable(A)) {
                            return this.findByName(A.name).filter(":checked").length
                        }
                }
                return D.length
            }, depend: function (D, A) {
                return this.dependTypes[typeof D] ? this.dependTypes[typeof D](D, A) : true
            }, dependTypes: {
                "boolean": function (D, A) {
                    return D
                }, string: function (D, A) {
                    return !!B(D, A.form).length
                }, "function": function (D, A) {
                    return D(A)
                }
            }, optional: function (A) {
                return !B.validator.methods.required.call(this, B.trim(A.value), A) && "dependency-mismatch"
            }, startRequest: function (A) {
                if (!this.pending[A.name]) {
                    this.pendingRequest++;
                    this.pending[A.name] = true
                }
            }, stopRequest: function (A, D) {
                this.pendingRequest--;
                if (this.pendingRequest < 0) {
                    this.pendingRequest = 0
                }
                delete this.pending[A.name];
                if (D && this.pendingRequest == 0 && this.formSubmitted && this.form()) {
                    B(this.currentForm).submit();
                    this.formSubmitted = false
                } else {
                    if (!D && this.pendingRequest == 0 && this.formSubmitted) {
                        B(this.currentForm).triggerHandler("invalid-form", [this]);
                        this.formSubmitted = false
                    }
                }
            }, previousValue: function (A) {
                return B.data(A, "previousValue") || B.data(A, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(A, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {required: true},
            email: {email: true},
            url: {url: true},
            date: {date: true},
            dateISO: {dateISO: true},
            number: {number: true},
            digits: {digits: true},
            creditcard: {creditcard: true}
        },
        addClassRules: function (A, D) {
            A.constructor == String ? this.classRuleSettings[A] = D : B.extend(this.classRuleSettings, A)
        },
        classRules: function (A) {
            var F = {};
            var E = B(A).attr("class");
            E && B.each(E.split(" "), function () {
                if (this in B.validator.classRuleSettings) {
                    B.extend(F, B.validator.classRuleSettings[this])
                }
            });
            return F
        },
        attributeRules: function (H) {
            var A = {};
            var I = B(H);
            for (var J in B.validator.methods) {
                var G;
                if (J === "required" && typeof B.fn.prop === "function") {
                    G = I.prop(J)
                } else {
                    G = I.attr(J)
                }
                if (G) {
                    A[J] = G
                } else {
                    if (I[0].getAttribute("type") === J) {
                        A[J] = true
                    }
                }
            }
            if (A.maxlength && /-1|2147483647|524288/.test(A.maxlength)) {
                delete A.maxlength
            }
            return A
        },
        metadataRules: function (A) {
            if (!B.metadata) {
                return {}
            }
            var D = B.data(A.form, "validator").settings.meta;
            return D ? B(A).metadata()[D] : B(A).metadata()
        },
        staticRules: function (A) {
            var F = {};
            var E = B.data(A.form, "validator");
            if (E.settings.rules) {
                F = B.validator.normalizeRule(E.settings.rules[A.name]) || {}
            }
            return F
        },
        normalizeRules: function (D, A) {
            B.each(D, function (C, G) {
                if (G === false) {
                    delete D[C];
                    return
                }
                if (G.param || G.depends) {
                    var H = true;
                    switch (typeof G.depends) {
                        case"string":
                            H = !!B(G.depends, A.form).length;
                            break;
                        case"function":
                            H = G.depends.call(A, A);
                            break
                    }
                    if (H) {
                        D[C] = G.param !== undefined ? G.param : true
                    } else {
                        delete D[C]
                    }
                }
            });
            B.each(D, function (F, C) {
                D[F] = B.isFunction(C) ? C(A) : C
            });
            B.each(["minlength", "maxlength", "min", "max"], function () {
                if (D[this]) {
                    D[this] = Number(D[this])
                }
            });
            B.each(["rangelength", "range"], function () {
                if (D[this]) {
                    D[this] = [Number(D[this][0]), Number(D[this][1])]
                }
            });
            if (B.validator.autoCreateRanges) {
                if (D.min && D.max) {
                    D.range = [D.min, D.max];
                    delete D.min;
                    delete D.max
                }
                if (D.minlength && D.maxlength) {
                    D.rangelength = [D.minlength, D.maxlength];
                    delete D.minlength;
                    delete D.maxlength
                }
            }
            if (D.messages) {
                delete D.messages
            }
            return D
        },
        normalizeRule: function (D) {
            if (typeof D == "string") {
                var A = {};
                B.each(D.split(/\s/), function () {
                    A[this] = true
                });
                D = A
            }
            return D
        },
        addMethod: function (E, F, A) {
            B.validator.methods[E] = F;
            B.validator.messages[E] = A != undefined ? A : B.validator.messages[E];
            if (F.length < 3) {
                B.validator.addClassRules(E, B.validator.normalizeRule(E))
            }
        },
        methods: {
            required: function (F, G, H) {
                if (!this.depend(H, G)) {
                    return "dependency-mismatch"
                }
                switch (G.nodeName.toLowerCase()) {
                    case"select":
                        var A = B(G).val();
                        return A && A.length > 0;
                    case"input":
                        if (this.checkable(G)) {
                            return this.getLength(F, G) > 0
                        }
                    default:
                        return B.trim(F).length > 0
                }
            }, remote: function (A, J, K) {
                if (this.optional(J)) {
                    return "dependency-mismatch"
                }
                var I = this.previousValue(J);
                if (!this.settings.messages[J.name]) {
                    this.settings.messages[J.name] = {}
                }
                I.originalMessage = this.settings.messages[J.name].remote;
                this.settings.messages[J.name].remote = I.message;
                K = typeof K == "string" && {url: K} || K;
                if (this.pending[J.name]) {
                    return "pending"
                }
                if (I.old === A) {
                    return I.valid
                }
                I.old = A;
                var L = this;
                this.startRequest(J);
                var H = {};
                H[J.name] = A;
                B.ajax(B.extend(true, {
                    url: K,
                    mode: "abort",
                    port: "validate" + J.name,
                    dataType: "json",
                    data: H,
                    success: function (F) {
                        L.settings.messages[J.name].remote = I.originalMessage;
                        var D = F === true;
                        if (D) {
                            var G = L.formSubmitted;
                            L.prepareElement(J);
                            L.formSubmitted = G;
                            L.successList.push(J);
                            L.showErrors()
                        } else {
                            var C = {};
                            var E = F || L.defaultMessage(J, "remote");
                            C[J.name] = I.message = B.isFunction(E) ? E(A) : E;
                            L.showErrors(C)
                        }
                        I.valid = D;
                        L.stopRequest(J, D)
                    }
                }, K));
                return "pending"
            }, minlength: function (A, E, F) {
                return this.optional(E) || this.getLength(B.trim(A), E) >= F
            }, maxlength: function (A, E, F) {
                return this.optional(E) || this.getLength(B.trim(A), E) <= F
            }, rangelength: function (A, G, H) {
                var F = this.getLength(B.trim(A), G);
                return this.optional(G) || (F >= H[0] && F <= H[1])
            }, min: function (A, E, F) {
                return this.optional(E) || A >= F
            }, max: function (A, E, F) {
                return this.optional(E) || A <= F
            }, range: function (A, E, F) {
                return this.optional(E) || (A >= F[0] && A <= F[1])
            }, email: function (D, A) {
                return this.optional(A) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(D)
            }, url: function (D, A) {
                return this.optional(A) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(D)
            }, date: function (D, A) {
                return this.optional(A) || !/Invalid|NaN/.test(new Date(D))
            }, dateISO: function (D, A) {
                return this.optional(A) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(D)
            }, number: function (D, A) {
                return this.optional(A) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(D)
            }, digits: function (D, A) {
                return this.optional(A) || /^\d+$/.test(D)
            }, creditcard: function (K, A) {
                if (this.optional(A)) {
                    return "dependency-mismatch"
                }
                if (/[^0-9 -]+/.test(K)) {
                    return false
                }
                var J = 0, L = 0, I = false;
                K = K.replace(/\D/g, "");
                for (var M = K.length - 1; M >= 0; M--) {
                    var N = K.charAt(M);
                    var L = parseInt(N, 10);
                    if (I) {
                        if ((L *= 2) > 9) {
                            L -= 9
                        }
                    }
                    J += L;
                    I = !I
                }
                return (J % 10) == 0
            }, accept: function (A, E, F) {
                F = typeof F == "string" ? F.replace(/,/g, "|") : "png|jpe?g|gif";
                return this.optional(E) || A.match(new RegExp(".(" + F + ")$", "i"))
            }, equalTo: function (F, G, H) {
                var A = B(H).unbind(".validate-equalTo").bind("blur.validate-equalTo", function () {
                    B(G).valid()
                });
                return F == A.val()
            }
        }
    });
    B.format = B.validator.format
})(jQuery);
(function (F) {
    var E = {};
    if (F.ajaxPrefilter) {
        F.ajaxPrefilter(function (A, B, H) {
            var C = A.port;
            if (A.mode == "abort") {
                if (E[C]) {
                    E[C].abort()
                }
                E[C] = H
            }
        })
    } else {
        var D = F.ajax;
        F.ajax = function (A) {
            var C = ("mode" in A ? A : F.ajaxSettings).mode, B = ("port" in A ? A : F.ajaxSettings).port;
            if (C == "abort") {
                if (E[B]) {
                    E[B].abort()
                }
                return (E[B] = D.apply(this, arguments))
            }
            return D.apply(this, arguments)
        }
    }
})(jQuery);
(function (B) {
    if (!jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener) {
        B.each({focus: "focusin", blur: "focusout"}, function (A, E) {
            B.event.special[E] = {
                setup: function () {
                    this.addEventListener(A, F, true)
                }, teardown: function () {
                    this.removeEventListener(A, F, true)
                }, handler: function (C) {
                    arguments[0] = B.event.fix(C);
                    arguments[0].type = E;
                    return B.event.handle.apply(this, arguments)
                }
            };
            function F(C) {
                C = B.event.fix(C);
                C.type = E;
                return B.event.handle.call(this, C)
            }
        })
    }
    B.extend(B.fn, {
        validateDelegate: function (F, A, E) {
            return this.bind(A, function (D) {
                var C = B(D.target);
                if (C.is(F)) {
                    return E.apply(C, arguments)
                }
            })
        }
    })
})(jQuery);
$(document).ready(function (B) {
    lenght()
});
function lenght() {
    var B = 0;
    $(".menu_four ul li:even,.menu_four ul li:odd").each(function () {
        if ($(this).height() > B) {
            B = $(this).height()
        }
    });
    $(".menu_four ul li:even,.menu_four ul li:odd").height(B)
}
function lenghtdiv() {
    var B = 0;
    $(".col_left,.col_right").each(function () {
        if ($(this).height() > B) {
            B = $(this).height()
        }
    });
    $(".col_left,.col_right").height(B)
};
(function ($) {
    "use strict";
    function setSelectionRange(rangeStart, rangeEnd) {
        if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveStart('character', rangeStart);
            range.moveEnd('character', rangeEnd - rangeStart);
            range.select();
        } else if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(rangeStart, rangeEnd);
        }
    }

    function getSelection(part) {
        var pos = this.value.length;
        part = (part.toLowerCase() == 'start' ? 'Start' : 'End');
        if (document.selection) {
            var range = document.selection.createRange(), stored_range, selectionStart, selectionEnd;
            stored_range = range.duplicate();
            stored_range.expand('textedit');
            stored_range.setEndPoint('EndToEnd', range);
            selectionStart = stored_range.text.length - range.text.length;
            selectionEnd = selectionStart + range.text.length;
            return part == 'Start' ? selectionStart : selectionEnd;
        } else if (typeof(this['selection' + part]) != "undefined") {
            pos = this['selection' + part];
        }
        return pos;
    }

    var _keydown = {
        codes: {
            46: 127,
            188: 44,
            109: 45,
            190: 46,
            191: 47,
            192: 96,
            220: 92,
            222: 39,
            221: 93,
            219: 91,
            173: 45,
            187: 61,
            186: 59,
            189: 45,
            110: 46
        },
        shifts: {
            96: "~",
            49: "!",
            50: "@",
            51: "#",
            52: "$",
            53: "%",
            54: "^",
            55: "&",
            56: "*",
            57: "(",
            48: ")",
            45: "_",
            61: "+",
            91: "{",
            93: "}",
            92: "|",
            59: ":",
            39: "\"",
            44: "<",
            46: ">",
            47: "?"
        }
    };
    $.fn.number = function (number, decimals, dec_point, thousands_sep) {
        thousands_sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
        dec_point = (typeof dec_point === 'undefined') ? '.' : dec_point;
        decimals = (typeof decimals === 'undefined') ? 0 : decimals;
        var u_dec = ('\\u' + ('0000' + (dec_point.charCodeAt(0).toString(16))).slice(-4)), regex_dec_num = new RegExp('[^' + u_dec + '0-9]', 'g'), regex_dec = new RegExp(u_dec, 'g');
        if (number === true) {
            if (this.is('input:text')) {
                return this.on({
                    'keydown.format': function (e) {
                        var $this = $(this), data = $this.data('numFormat'), code = (e.keyCode ? e.keyCode : e.which), chara = '', start = getSelection.apply(this, ['start']), end = getSelection.apply(this, ['end']), val = '', setPos = false;
                        if (_keydown.codes.hasOwnProperty(code)) {
                            code = _keydown.codes[code];
                        }
                        if (!e.shiftKey && (code >= 65 && code <= 90)) {
                            code += 32;
                        } else if (!e.shiftKey && (code >= 69 && code <= 105)) {
                            code -= 48;
                        } else if (e.shiftKey && _keydown.shifts.hasOwnProperty(code)) {
                            chara = _keydown.shifts[code];
                        }
                        if (chara == '')chara = String.fromCharCode(code);
                        if (code != 8 && code != 45 && code != 127 && chara != dec_point && !chara.match(/[0-9]/)) {
                            var key = (e.keyCode ? e.keyCode : e.which);
                            if (key == 46 || key == 8 || key == 127 || key == 9 || key == 27 || key == 13 || ((key == 65 || key == 82 || key == 80 || key == 83 || key == 70 || key == 72 || key == 66 || key == 74 || key == 84 || key == 90 || key == 61 || key == 173 || key == 48) && (e.ctrlKey || e.metaKey) === true) || ((key == 86 || key == 67 || key == 88) && (e.ctrlKey || e.metaKey) === true) || ((key >= 35 && key <= 39)) || ((key >= 112 && key <= 123))) {
                                return;
                            }
                            e.preventDefault();
                            return false;
                        }
                        if (start == 0 && end == this.value.length) {
                            if (code == 8) {
                                start = end = 1;
                                this.value = '';
                                data.init = (decimals > 0 ? -1 : 0);
                                data.c = (decimals > 0 ? -(decimals + 1) : 0);
                                setSelectionRange.apply(this, [0, 0]);
                            } else if (chara == dec_point) {
                                start = end = 1;
                                this.value = '0' + dec_point + (new Array(decimals + 1).join('0'));
                                data.init = (decimals > 0 ? 1 : 0);
                                data.c = (decimals > 0 ? -(decimals + 1) : 0);
                            } else if (code == 45) {
                                start = end = 2;
                                this.value = '-0' + dec_point + (new Array(decimals + 1).join('0'));
                                data.init = (decimals > 0 ? 1 : 0);
                                data.c = (decimals > 0 ? -(decimals + 1) : 0);
                                setSelectionRange.apply(this, [2, 2]);
                            } else {
                                data.init = (decimals > 0 ? -1 : 0);
                                data.c = (decimals > 0 ? -(decimals) : 0);
                            }
                        } else {
                            data.c = end - this.value.length;
                        }
                        data.isPartialSelection = start == end ? false : true;
                        if (decimals > 0 && chara == dec_point && start == this.value.length - decimals - 1) {
                            data.c++;
                            data.init = Math.max(0, data.init);
                            e.preventDefault();
                            setPos = this.value.length + data.c;
                        } else if (code == 45 && (start != 0 || this.value.indexOf('-') == 0)) {
                            e.preventDefault();
                        } else if (chara == dec_point) {
                            data.init = Math.max(0, data.init);
                            e.preventDefault();
                        } else if (decimals > 0 && code == 127 && start == this.value.length - decimals - 1) {
                            e.preventDefault();
                        } else if (decimals > 0 && code == 8 && start == this.value.length - decimals) {
                            e.preventDefault();
                            data.c--;
                            setPos = this.value.length + data.c;
                        } else if (decimals > 0 && code == 127 && start > this.value.length - decimals - 1) {
                            if (this.value === '')return;
                            if (this.value.slice(start, start + 1) != '0') {
                                val = this.value.slice(0, start) + '0' + this.value.slice(start + 1);
                                $this.val(val);
                            }
                            e.preventDefault();
                            setPos = this.value.length + data.c;
                        } else if (decimals > 0 && code == 8 && start > this.value.length - decimals) {
                            if (this.value === '')return;
                            if (this.value.slice(start - 1, start) != '0') {
                                val = this.value.slice(0, start - 1) + '0' + this.value.slice(start);
                                $this.val(val);
                            }
                            e.preventDefault();
                            data.c--;
                            setPos = this.value.length + data.c;
                        } else if (code == 127 && this.value.slice(start, start + 1) == thousands_sep) {
                            e.preventDefault();
                        } else if (code == 8 && this.value.slice(start - 1, start) == thousands_sep) {
                            e.preventDefault();
                            data.c--;
                            setPos = this.value.length + data.c;
                        } else if (decimals > 0 && start == end && this.value.length > decimals + 1 && start > this.value.length - decimals - 1 && isFinite(+chara) && !e.metaKey && !e.ctrlKey && !e.altKey && chara.length === 1) {
                            if (end === this.value.length) {
                                val = this.value.slice(0, start - 1);
                            } else {
                                val = this.value.slice(0, start) + this.value.slice(start + 1);
                            }
                            this.value = val;
                            setPos = start;
                        }
                        if (setPos !== false) {
                            setSelectionRange.apply(this, [setPos, setPos]);
                        }
                        $this.data('numFormat', data);
                    }, 'keyup.format': function (e) {
                        var $this = $(this), data = $this.data('numFormat'), code = (e.keyCode ? e.keyCode : e.which), start = getSelection.apply(this, ['start']), end = getSelection.apply(this, ['end']), setPos;
                        if (start === 0 && end === 0 && (code === 189 || code === 109)) {
                            $this.val('-' + $this.val());
                            start = 1;
                            data.c = 1 - this.value.length;
                            data.init = 1;
                            $this.data('numFormat', data);
                            setPos = this.value.length + data.c;
                            setSelectionRange.apply(this, [setPos, setPos]);
                        }
                        if (this.value === '' || (code < 48 || code > 57) && (code < 96 || code > 105) && code !== 8 && code !== 46 && code !== 110)return;
                        $this.val($this.val());
                        if (decimals > 0) {
                            if (data.init < 1) {
                                start = this.value.length - decimals - (data.init < 0 ? 1 : 0);
                                data.c = start - this.value.length;
                                data.init = 1;
                                $this.data('numFormat', data);
                            } else if (start > this.value.length - decimals && code != 8) {
                                data.c++;
                                $this.data('numFormat', data);
                            }
                        }
                        if (code == 46 && !data.isPartialSelection) {
                            data.c++;
                            $this.data('numFormat', data);
                        }
                        setPos = this.value.length + data.c;
                        setSelectionRange.apply(this, [setPos, setPos]);
                    }, 'paste.format': function (e) {
                        var $this = $(this), original = e.originalEvent, val = null;
                        if (window.clipboardData && window.clipboardData.getData) {
                            val = window.clipboardData.getData('Text');
                        } else if (original.clipboardData && original.clipboardData.getData) {
                            val = original.clipboardData.getData('text/plain');
                        }
                        $this.val(val);
                        e.preventDefault();
                        return false;
                    }
                }).each(function () {
                    var $this = $(this).data('numFormat', {
                        c: -(decimals + 1),
                        decimals: decimals,
                        thousands_sep: thousands_sep,
                        dec_point: dec_point,
                        regex_dec_num: regex_dec_num,
                        regex_dec: regex_dec,
                        init: this.value.indexOf('.') ? true : false
                    });
                    if (this.value === '')return;
                    $this.val($this.val());
                });
            } else {
                return this.each(function () {
                    var $this = $(this), num = +$this.text().replace(regex_dec_num, '').replace(regex_dec, '.');
                    $this.number(!isFinite(num) ? 0 : +num, decimals, dec_point, thousands_sep);
                });
            }
        }
        return this.text($.number.apply(window, arguments));
    };
    var origHookGet = null, origHookSet = null;
    if ($.isPlainObject($.valHooks.text)) {
        if ($.isFunction($.valHooks.text.get))origHookGet = $.valHooks.text.get;
        if ($.isFunction($.valHooks.text.set))origHookSet = $.valHooks.text.set;
    } else {
        $.valHooks.text = {};
    }
    $.valHooks.text.get = function (el) {
        var $this = $(el), num, negative, data = $this.data('numFormat');
        if (!data) {
            if ($.isFunction(origHookGet)) {
                return origHookGet(el);
            } else {
                return undefined;
            }
        } else {
            if (el.value === '')return '';
            num = +(el.value.replace(data.regex_dec_num, '').replace(data.regex_dec, '.'));
            return (el.value.indexOf('-') === 0 ? '-' : '') + (isFinite(num) ? num : 0);
        }
    };
    $.valHooks.text.set = function (el, val) {
        var $this = $(el), data = $this.data('numFormat');
        if (!data) {
            if ($.isFunction(origHookSet)) {
                return origHookSet(el, val);
            } else {
                return undefined;
            }
        } else {
            var num = $.number(val, data.decimals, data.dec_point, data.thousands_sep);
            return $.isFunction(origHookSet) ? origHookSet(el, num) : el.value = num;
        }
    };
    $.number = function (number, decimals, dec_point, thousands_sep) {
        thousands_sep = (typeof thousands_sep === 'undefined') ? (new Number(1000).toLocaleString() !== '1000' ? new Number(1000).toLocaleString().charAt(1) : '') : thousands_sep;
        dec_point = (typeof dec_point === 'undefined') ? new Number(0.1).toLocaleString().charAt(1) : dec_point;
        decimals = !isFinite(+decimals) ? 0 : Math.abs(decimals);
        var u_dec = ('\\u' + ('0000' + (dec_point.charCodeAt(0).toString(16))).slice(-4));
        var u_sep = ('\\u' + ('0000' + (thousands_sep.charCodeAt(0).toString(16))).slice(-4));
        number = (number + '').replace('\.', dec_point).replace(new RegExp(u_sep, 'g'), '').replace(new RegExp(u_dec, 'g'), '.').replace(new RegExp('[^0-9+\-Ee.]', 'g'), '');
        var n = !isFinite(+number) ? 0 : +number, s = '', toFixedFix = function (n, decimals) {
            return '' + (+(Math.round(n + 'e+' + decimals) + 'e-' + decimals));
        };
        s = (decimals ? toFixedFix(n, decimals) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, thousands_sep);
        }
        if ((s[1] || '').length < decimals) {
            s[1] = s[1] || '';
            s[1] += new Array(decimals - s[1].length + 1).join('0');
        }
        return s.join(dec_point);
    }
})(jQuery);
!function (l) {
    l.fn.imageScale = function (b) {
        return this.each(function () {
            var f = this, c = l(this), a = c.data("imageScale"), g = "IMG" === this.tagName ? c : c.find("img");
            if (a)if ("string" == typeof b)a[b](); else if ("object" == typeof b)a[b.method || "scale"](!1, b); else a.scale(); else {
                var e = g[0].complete, k = l.extend({}, l.fn.imageScale.defaults, "object" == typeof b && b), h = function () {
                    c.data("imageScale", a = new n(f, k));
                    a.scale(!0, k)
                };
                e ? h.apply(c[0]) : g.on("load", h).attr("src", g.attr("src"))
            }
        })
    };
    l.fn.imageScale.defaults = {
        scale: "best-fill",
        align: "center",
        parent: null,
        hideParentOverflow: !0,
        fadeInDuration: 0,
        rescaleOnResize: !1,
        didScale: function (b, f) {
        },
        logLevel: 0
    };
    var n = function (b, f) {
        var c = this;
        c.options = f;
        c.element = b;
        var a = c.$element = l(b), g = c.$img = "IMG" === b.tagName ? a : a.find("img"), e = c.img = g[0];
        c.src = g.attr("src");
        c.imgWidth = e.naturalWidth || e.width;
        c.imgHeight = e.naturalHeight || e.height;
        a = c.$parent = f.parent ? f.parent : l(a.parent()[0]);
        c.parent = a[0];
        "static" === a.css("position") && a.css("position", "relative");
        f.rescaleOnResize && l(window).resize(function (a) {
            c.scheduleScale()
        })
    };
    l.fn.imageScale.Constructor = n;
    n.prototype = {
        NONE: "none",
        FILL: "fill",
        BEST_FILL: "best-fill",
        BEST_FIT: "best-fit",
        BEST_FIT_DOWN_ONLY: "best-fit-down",
        ALIGN_LEFT: "left",
        ALIGN_RIGHT: "right",
        ALIGN_CENTER: "center",
        ALIGN_TOP: "top",
        ALIGN_BOTTOM: "bottom",
        ALIGN_TOP_LEFT: "top-left",
        ALIGN_TOP_RIGHT: "top-right",
        ALIGN_BOTTOM_LEFT: "bottom-left",
        ALIGN_BOTTOM_RIGHT: "bottom-right",
        constructor: n,
        element: null,
        options: null,
        scale: function (b, f) {
            if (!this._isDestroyed && !1 !== this._canScale) {
                var c = this, a = this.options, g = this.$parent,
                    e = this.element, k = this.$element, h = this.$img;
                if (b)a.hideParentOverflow && g.css({overflow: "hidden"}); else if (this.src !== h.attr("src")) {
                    this.destroy();
                    k.data("imageScale", null);
                    k.imageScale(a);
                    return
                }
                this._didScheduleScale = !1;
                if (!a.rescaleOnResize || f || this._needUpdate(this.parent)) {
                    f = f ? f : {};
                    if (h = f.transition)this._canScale = !1, k.css("transition", "all " + h + "ms"), setTimeout(function () {
                        c._canScale = null;
                        k.css("transition", "null")
                    }, h);
                    var h = f.destWidth ? f.destWidth : g.outerWidth(), d = f.destHeight ? f.destHeight : g.outerHeight(),
                        l = f.destWidth ? f.destWidth : g.innerWidth(), m = f.destHeight ? f.destHeight : g.innerHeight(), g = h - l, l = d - m, m = k.attr("data-scale"), p = k.attr("data-align"), m = m ? m : a.scale, n = p ? p : a.align, p = a.fadeInDuration;
                    if (m) {
                        this._cacheDestWidth === h && this._cacheDestHeight === d && 2 < a.logLevel && console.log("imageScale - DEBUG NOTICE: The parent size hasn't changed: dest width: '" + h + "' - dest height: '" + d + "'.", e);
                        var q = this.imgWidth, r = this.imgHeight;
                        h && d && q && r ? (this._cacheDestWidth = h, this._cacheDestHeight = d, e = this._innerFrameForSize(m,
                            n, q, r, h, d), g && (e.x -= g / 2), l && (e.y -= l / 2), k.css({
                            position: "absolute",
                            top: e.y + "px",
                            left: e.x + "px",
                            width: e.width + "px",
                            height: e.height + "px",
                            "max-width": "none"
                        }), b && p && (k.css({display: "none"}), k.fadeIn(p)), a.didScale.call(this, b, f)) : 0 < a.logLevel && console.error("imageScale - DEBUG ERROR: The dimensions are incorrect: source width: '" + q + "' - source height: '" + r + "' - dest width: '" + h + "' - dest height: '" + d + "'.", e)
                    } else 2 < a.logLevel && console.log("imageScale - DEBUG NOTICE: The scale property is null.", e)
                }
            }
        },
        destroy: function () {
            this._isDestroyed = !0;
            this.$element.removeData("imageScale")
        },
        _innerFrameForSize: function (b, f, c, a, g, e) {
            var k, h, d;
            d = {x: 0, y: 0, width: g, height: e};
            if (b === this.FILL)return d;
            k = g / c;
            h = e / a;
            switch (b) {
                case this.BEST_FIT_DOWN_ONLY:
                    b !== this.BEST_FIT_DOWN_ONLY && 1 < this.options.logLevel && console.warn("imageScale - DEBUG WARNING: The scale '" + b + "' was not understood.");
                    b = c > g || a > e ? k < h ? k : h : 1;
                    break;
                case this.BEST_FIT:
                    b = k < h ? k : h;
                    break;
                case this.NONE:
                    b = 1;
                    break;
                default:
                    b = k > h ? k : h
            }
            c *= b;
            a *= b;
            d.width = Math.round(c);
            d.height = Math.round(a);
            switch (f) {
                case this.ALIGN_LEFT:
                    d.x = 0;
                    d.y = e / 2 - a / 2;
                    break;
                case this.ALIGN_RIGHT:
                    d.x = g - c;
                    d.y = e / 2 - a / 2;
                    break;
                case this.ALIGN_TOP:
                    d.x = g / 2 - c / 2;
                    d.y = 0;
                    break;
                case this.ALIGN_BOTTOM:
                    d.x = g / 2 - c / 2;
                    d.y = e - a;
                    break;
                case this.ALIGN_TOP_LEFT:
                    d.x = 0;
                    d.y = 0;
                    break;
                case this.ALIGN_TOP_RIGHT:
                    d.x = g - c;
                    d.y = 0;
                    break;
                case this.ALIGN_BOTTOM_LEFT:
                    d.x = 0;
                    d.y = e - a;
                    break;
                case this.ALIGN_BOTTOM_RIGHT:
                    d.x = g - c;
                    d.y = e - a;
                    break;
                default:
                    f !== this.ALIGN_CENTER && 1 < this.options.logLevel && console.warn("imageScale - DEBUG WARNING: The align '" +
                    f + "' was not understood."), d.x = g / 2 - c / 2, d.y = e / 2 - a / 2
            }
            return d
        },
        _needUpdate: function (b) {
            b = b.clientHeight + " " + b.clientWidth;
            return this._lastParentSize !== b ? (this._lastParentSize = b, !0) : !1
        },
        scheduleScale: function () {
            if (!this._didScheduleScale)if (window.requestAnimationFrame) {
                var b = this;
                this._didScheduleScale = !0;
                requestAnimationFrame(function () {
                    setTimeout(function () {
                        b.scale()
                    }, 0)
                })
            } else this.scale()
        }
    }
}(window.jQuery);
!function (a) {
    a.fn.unveil = function (b, c) {
        function d() {
            var b = j.filter(function () {
                var b = a(this);
                if (!b.is(":hidden")) {
                    var c = f.scrollTop(), d = c + f.height(), e = b.offset().top, h = e + b.height();
                    return h >= c - g && d + g >= e
                }
            });
            e = b.trigger("unveil"), j = j.not(e)
        }

        var e, f = a(window), g = b || 0, h = window.devicePixelRatio > 1, i = h ? "data-src-retina" : "data-src", j = this;
        return this.one("unveil", function () {
            var a = this.getAttribute(i);
            a = a || this.getAttribute("data-src"), a && (this.setAttribute("src", a), "function" == typeof c && c.call(this))
        }), f.on("scroll.unveil resize.unveil lookup.unveil", d), d(), this
    }
}(window.jQuery || window.Zepto);
var gMap = function () {
    var a, b = function (b) {
        var c = document.createElement("script");
        c.type = "text/javascript", c.src = b, $("#" + a.id).html(c)
    }, c = function (b) {
        a = b
    }, d = function (a) {
        var b = new google.maps.LatLng(a.latitudes, a.longitudes, a.zoom), c = {
            center: b,
            zoom: a.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }, d = new google.maps.Map(document.getElementById(a.id), c);
        e({latlong: b, title: a.title, map: d, icon: a.icon})
    }, e = function (a) {
        new google.maps.Marker({position: a.latlong, title: a.title, map: a.map, icon: a.icon})
    }, f = function () {
        d(a)
    };
    return {setParams: c, addLibrary: b, init: f}
};
var Application = void 0, Application = Application || {};

$("#module-slider-gold .item").on("click", function () {
    var data_url = $(this).attr('data-url');
    window.location.assign(data_url)
})