<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 

	<div id="breadcrumbs">

        <div class="container">
            <div class="row">
                <?php
                    echo do_shortcode('[breadcrumb]');
                ?>
            </div>
        </div>

      </div>

<div id="danhsach-tour">

		<div class="container">
			<div class="row">

                <div class="archive-title">
                    <h1>Các tour du lịch trong nước và quốc tế hấp dẫn</h1>
                </div>

				<div class="col-xs-12 col-lg-9 col-md-9">
					<div class="row">
                        <div class="list-tour">
                            <!-- danh sach cac tour -->
                            <?php if(have_posts()):while(have_posts()):the_post();?>
                                <div class="cac-tour">

                                    <div class="col-lg-4 col-xs-12">
                                        <div class="row">
                                            <div class="img">
                                                <a rel="nofollow" href="<?php the_permalink();?>" title="<?php the_title();?>"><img width="216" height="185" src="<?php echo get_the_post_thumbnail_url()?>" class="attachment-tour_cat wp-post-image" alt="<?php the_title();?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-xs-12">
                                        <div class="row">
                                            <div class="thongtin-tour">

                                                <div class="div1">

                                                    <div class="ten-tour">

                                                        <h2><a href="<?php the_permalink();?>" title="<?php the_title();?>" data-tour="1">
                                                                <?php the_title();?>
                                                            </a></h2>

                                                        <h6>

                                                            <i class="fa-tint"></i>

                                                            <?php $thoi_gian = get_post_meta( get_the_ID(), 'wpcf-thoi-gian', true ); echo $thoi_gian;?>
                                                            <div class="star">
                                                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>										    </div>

                                                        </h6>

                                                    </div>

                                                </div>

                                                <div class="div2">

                                                    <div class="mota">

                                                        <?php the_excerpt();?>
                                                        <i class="fa fa-check"></i>
                                                        <i class="fa fa-street-view"></i>
                                                        <i class="fa fa-car"></i>
                                                        <i class="fa fa-ticket"></i>
                                                    </div>

                                                    <div class="gia">

                                                        <h3 class="gia_tour" data-tour="1">
                                                            <?php
                                                            $key_1_value = get_post_meta( get_the_ID(), 'wpcf-gia', true );
                                                            // Check if the custom field has a value.
                                                            if ( ! empty( $key_1_value ) ) {
                                                                echo $key_1_value;
                                                            }
                                                            ?>
                                                        </h3>

                                                        <center>(VND)</center>
                                                        <?php
                                                        $ten = get_the_title();

                                                        $gia = get_post_meta( get_the_ID(), 'wpcf-gia', true );

                                                        $diem_den = get_post_meta( get_the_ID(), 'wpcf-diem-den', true );



                                                        $khoi_hanh = get_post_meta( get_the_ID(), 'wpcf-khoi-hanh', true );
                                                        ?>
                                                        <?php
                                                        $link = "&ten=".$ten."&gia=".$gia."&thoi_gian=".$thoi_gian;
                                                        ?>
                                                        <a class="btn dat_tour" href="<?php echo get_home_url().'/?page_id=28'.$link;?>">Đặt tour <i class="icon fa fa-chevron-right"></i></a>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php endwhile;endif;wp_reset_query();?>
                            <div id="paginate">
                                <div class="wp-pagenavi">
                                    <?php wp_pagenavi();?>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>

				<div class="col-xs-12 col-lg-3 col-md-3">
					<div class="row">
                        <div class="right-col orange">

                            <div class="header">
                                Gọi để được tư vấn
                            </div>

                            <div class="line"></div>

                            <img class="tvdt_img" src="<?php echo get_stylesheet_directory_uri().'/img/121.png'?>" alt="Liên hệ tư vấn" onclick="_gaq.push(['_trackEvent', 'mWebCall', 'Click2Call']);">

                            <p>Hoặc để lại số điện thoại, Ngọc Nguyên Travel sẽ gọi cho bạn</p>

                            <div role="form" class="wpcf7" id="wpcf7-f531-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/tour#wpcf7-f531-o1" method="post" class="wpcf7-form ng-pristine ng-valid" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="531">
                                        <input type="hidden" name="_wpcf7_version" value="4.2.2">
                                        <input type="hidden" name="_wpcf7_locale" value="en_US">
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f531-o1">
                                        <input type="hidden" name="_wpnonce" value="2fb346123b">
                                    </div>
                                    <p><span class="wpcf7-form-control-wrap tel-904"><input type="tel" name="tel-904" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại của tôi là..."></span><br>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal-tuvan">Tiếp tục</button></p>
                                    <div class="modal modal-tuvan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <p>Vui lòng nhập mã xác thực (phân biệt chữ hoa/chữ thường)</p>
                                                <p>    <input type="hidden" name="_wpcf7_captcha_challenge_captcha-867" value="138297314"><img class="wpcf7-form-control wpcf7-captchac wpcf7-captcha-captcha-867" width="72" height="24" alt="captcha" src="http://pystravel.vn/wp-content/uploads/wpcf7_captcha/138297314.png"><span class="wpcf7-form-control-wrap captcha-867"><input type="text" name="captcha-867" value="" size="40" class="wpcf7-form-control wpcf7-captchar" aria-invalid="false" placeholder="Mã xác thực"></span><br>
                                                    <input type="submit" value="Hãy gọi lại cho tôi" class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="http://pystravel.vn/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;">
                                                </p></div>
                                            <p style="height: 20px;"></p></div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
                        </div>
                    </div>
				</div>

			</div>
		</div>

	  </div>

	
	<?php get_footer(); ?>